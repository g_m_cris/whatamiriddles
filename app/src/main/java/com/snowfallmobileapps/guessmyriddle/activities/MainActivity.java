package com.snowfallmobileapps.guessmyriddle.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.snowfallmobileapps.guessmyriddle.R;
import com.snowfallmobileapps.guessmyriddle.models.Money;
import com.snowfallmobileapps.guessmyriddle.util.WhatAmIRiddlesUtil;
import com.snowfallmobileapps.guessmyriddle.util.billing.IabHelper;
import com.snowfallmobileapps.guessmyriddle.util.billing.IabResult;
import com.snowfallmobileapps.guessmyriddle.util.billing.Purchase;

public class MainActivity extends ActionBarActivity {
    private Animator mCurrentAnimator;
    private int mShortAnimationDuration;
    private ImageView expandedImageView;
    private View thumb1View;

    private IabHelper mHelper;

    public final static String REMOVE_ADS = "remove_ads";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boolean areAdsRemoved = WhatAmIRiddlesUtil.areAdsRemoved(getApplicationContext());
        if (!areAdsRemoved) {
            setUpInAppPurchase();
        }

        TextView bagOfMoneyTextView = (TextView) findViewById(R.id.bagOfMoneyTextView);
        bagOfMoneyTextView.setText("You have " + Money.getBagOfMoney(getApplicationContext()) + " coins ");

        expandedImageView = (ImageView) findViewById(R.id.door);
        expandedImageView.setVisibility(View.GONE);

        thumb1View = findViewById(R.id.thumb_door);
        thumb1View.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //thumb1View.setBackground(Drawable.createFromPath("@drawable/door"));
                zoomImageFromThumb(thumb1View, R.drawable.door);
            }
        });

        // Retrieve and cache the system's default "short" animation time.
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime)+1500;

        //admob
        /*
        AdView adView = (AdView) this.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(getString(R.string.admob_test_devices)).build();
        adView.loadAd(adRequest);
        */
    }

    private void setUpInAppPurchase() {
        // compute your public key and store it in base64EncodedPublicKey
        mHelper = new IabHelper(this, Money.base64EncodedPublicKey);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    complain("Problem setting up in-app billing: " + result);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;
            }
        });
    }

    private void complain(String message) {
        alert("Error: " + message);
    }

    private void alert(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        bld.create().show();
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener =
            new IabHelper.OnIabPurchaseFinishedListener() {
                public void onIabPurchaseFinished(IabResult result, Purchase purchase)
                {
                    // if we were disposed of in the meantime, quit.
                    if (mHelper == null) {
                        setWaitScreen(false);
                        return;
                    }

                    if (result.isFailure()) {
                        setWaitScreen(false);
                        complain("Error purchasing: " + result);
                        return;
                    }

                    if (!verifyDeveloperPayload(purchase)) {
                        setWaitScreen(false);
                        complain("Error purchasing. Authenticity verification failed.");
                        return;
                    }

                    WhatAmIRiddlesUtil.setAdsRemoved(getApplicationContext());
                }
            };

    /** Verifies the developer payload of a purchase. */
    boolean verifyDeveloperPayload(Purchase purchase) {
        String payload = purchase.getDeveloperPayload();
        boolean result = false;

        if (purchase.getSku().equals(REMOVE_ADS)) {
            if (payload.equals(REMOVE_ADS)) {
                result = true;
            }
        }

        return result;
    }

    // Enables or disables the "please wait" screen.
    void setWaitScreen(boolean set) {
        //findViewById(R.id.screen_main).setVisibility(set ? View.GONE : View.VISIBLE);
        //findViewById(R.id.screen_wait).setVisibility(set ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mHelper == null) return;

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void zoomImageFromThumb(final View thumbView, int imageResId) {
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        // Load the high-resolution "zoomed-in" image.
        expandedImageView.setImageResource(imageResId);

        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        findViewById(R.id.container)
                .getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                        startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
                View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
                Intent newIntent = new Intent(MainActivity.this, LevelsActivity.class);
                startActivity(newIntent);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Always call the superclass method first
        if (expandedImageView.getVisibility() != View.GONE) {
            thumb1View.setAlpha(1f);
            expandedImageView.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_help) {
            WhatAmIRiddlesUtil.help(this);
            return true;
        }
        if (id == R.id.action_rate) {
            WhatAmIRiddlesUtil.rateApp(this);
            return true;
        }
        if (id == R.id.action_share) {
            WhatAmIRiddlesUtil.shareApp(this);
            return true;
        }
        if (id == R.id.action_remove_ads) {
            boolean areAdsRemoved = WhatAmIRiddlesUtil.areAdsRemoved(getApplicationContext());
            if (!areAdsRemoved) {
                setWaitScreen(true);
                if (mHelper != null) {
                    mHelper.launchPurchaseFlow(MainActivity.this, REMOVE_ADS, 1, mPurchaseFinishedListener, REMOVE_ADS);
                }
            }
            else {
                showOkDialog("No Ads", "Ads are already removed. Thank you!");
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showOkDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_alert))
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //simply return
                    }
                }).show();
    }
}
