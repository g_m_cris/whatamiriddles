package com.snowfallmobileapps.guessmyriddle.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.snowfallmobileapps.guessmyriddle.R;
import com.snowfallmobileapps.guessmyriddle.models.Money;
import com.snowfallmobileapps.guessmyriddle.util.WhatAmIRiddlesUtil;
import com.snowfallmobileapps.guessmyriddle.util.billing.IabHelper;
import com.snowfallmobileapps.guessmyriddle.util.billing.IabResult;
import com.snowfallmobileapps.guessmyriddle.util.billing.Inventory;
import com.snowfallmobileapps.guessmyriddle.util.billing.Purchase;

public class PurchaseCoinsActivity extends ActionBarActivity {

    public final static String COINS_50 = "coins_50";
    public final static String COINS_100 = "coins_100";
    public final static String COINS_200 = "coins_200";
    public final static String COINS_400 = "coins_400";
    public final static String REMOVE_ADS = "remove_ads";

    private IabHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_coins);

        setUpInAppPurchase();

        LinearLayout coins50 = (LinearLayout) findViewById(R.id.coins50Layout);
        coins50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setWaitScreen(true);
                if (mHelper != null) {
                    mHelper.launchPurchaseFlow(PurchaseCoinsActivity.this, COINS_50, 1, mPurchaseFinishedListener, COINS_50);
                }
            }
        });

        LinearLayout coins110 = (LinearLayout) findViewById(R.id.coins110Layout);
        coins110.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setWaitScreen(true);
                if (mHelper != null) {
                    mHelper.launchPurchaseFlow(PurchaseCoinsActivity.this, COINS_100, 2, mPurchaseFinishedListener, COINS_100);
                }
            }
        });

        LinearLayout coins240 = (LinearLayout) findViewById(R.id.coins240Layout);
        coins240.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setWaitScreen(true);
                if (mHelper != null) {
                    mHelper.launchPurchaseFlow(PurchaseCoinsActivity.this, COINS_200, 3, mPurchaseFinishedListener, COINS_200);
                }
            }
        });

        LinearLayout coins520 = (LinearLayout) findViewById(R.id.coins520Layout);
        coins520.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setWaitScreen(true);
                if (mHelper != null) {
                    mHelper.launchPurchaseFlow(PurchaseCoinsActivity.this, COINS_400, 4, mPurchaseFinishedListener, COINS_400);
                }
            }
        });

        //admob
        /*
        AdView adView = (AdView) this.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(getString(R.string.admob_test_devices)).build();
        adView.loadAd(adRequest);
        */
    }

    private void updateBagOfMoney(Purchase purchase) {
        // provision the in-app purchase to the user
        // (for example, credit 50 gold coins to player's character)
        int coinsPurchased = 0;
        if (purchase.getSku().equals(COINS_50)) {
            coinsPurchased = 50;
        }
        else if (purchase.getSku().equals(COINS_100)) {
            coinsPurchased = 100;
        }
        else if (purchase.getSku().equals(COINS_200)) {
            coinsPurchased = 200;
        }
        else if (purchase.getSku().equals(COINS_400)) {
            coinsPurchased = 400;
        }
        Money.increaseBagOfMoney(coinsPurchased, getApplicationContext());
    }

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
            new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            if (result.isSuccess()) {
                //update bag of money
                updateBagOfMoney(purchase);
            }
            else {
                // handle error
                complain("Error purchasing: " + result);
            }
            setWaitScreen(false);
        }
    };

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener =
            new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase)
        {
            // if we were disposed of in the meantime, quit.
            if (mHelper == null) {
                setWaitScreen(false);
                return;
            }

            if (result.isFailure()) {
                setWaitScreen(false);
                complain("Error purchasing: " + result);
                return;
            }

            if (!verifyDeveloperPayload(purchase)) {
                setWaitScreen(false);
                complain("Error purchasing. Authenticity verification failed.");
                return;
            }

            if (purchase.getSku().equals(REMOVE_ADS)) {
                WhatAmIRiddlesUtil.setAdsRemoved(getApplicationContext());
            }
            else {
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            }
        }
    };

    /** Verifies the developer payload of a purchase. */
    boolean verifyDeveloperPayload(Purchase purchase) {
        String payload = purchase.getDeveloperPayload();
        boolean result = false;

        if (purchase.getSku().equals(COINS_50)) {
            if (payload.equals(COINS_50)) {
                result = true;
            }
        }
        else if (purchase.getSku().equals(COINS_100)) {
            if (payload.equals(COINS_100)) {
                result = true;
            }
        }
        else if (purchase.getSku().equals(COINS_200)) {
            if (payload.equals(COINS_200)) {
                result = true;
            }
        }
        else if (purchase.getSku().equals(COINS_400)) {
            if (payload.equals(COINS_400)) {
                result = true;
            }
        }
        else if (purchase.getSku().equals(REMOVE_ADS)) {
            if (payload.equals(REMOVE_ADS)) {
                result = true;
            }
        }

        return result;
    }

    private void setUpInAppPurchase() {
        // compute your public key and store it in base64EncodedPublicKey
        mHelper = new IabHelper(this, Money.base64EncodedPublicKey);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    complain("Problem setting up in-app billing: " + result);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                mHelper.queryInventoryAsync(mGotInventoryListener);
            }
        });
    }

    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                complain("Failed to query inventory. Please check your internet connexion.");
                return;
            }

            // Check for coins -- if we own coins, we should consume them immediately
            Purchase coinsPurchase50 = inventory.getPurchase(COINS_50);
            if (coinsPurchase50 != null && verifyDeveloperPayload(inventory.getPurchase(COINS_50))) {
                mHelper.consumeAsync(inventory.getPurchase(COINS_50), mConsumeFinishedListener);
                return;
            }

            Purchase coinsPurchase100 = inventory.getPurchase(COINS_100);
            if (coinsPurchase100 != null && verifyDeveloperPayload(inventory.getPurchase(COINS_100))) {
                mHelper.consumeAsync(inventory.getPurchase(COINS_100), mConsumeFinishedListener);
                return;
            }

            Purchase coinsPurchase200 = inventory.getPurchase(COINS_200);
            if (coinsPurchase200 != null && verifyDeveloperPayload(inventory.getPurchase(COINS_200))) {
                mHelper.consumeAsync(inventory.getPurchase(COINS_200), mConsumeFinishedListener);
                return;
            }

            Purchase coinsPurchase400 = inventory.getPurchase(COINS_400);
            if (coinsPurchase400 != null && verifyDeveloperPayload(inventory.getPurchase(COINS_400))) {
                mHelper.consumeAsync(inventory.getPurchase(COINS_400), mConsumeFinishedListener);
                return;
            }

            Purchase removeAdsPurchase = inventory.getPurchase(REMOVE_ADS);
            if (removeAdsPurchase != null && verifyDeveloperPayload(inventory.getPurchase(REMOVE_ADS))) {
                WhatAmIRiddlesUtil.setAdsRemoved(getApplicationContext());
                return;
            }

            setWaitScreen(false);
        }
    };

    private void complain(String message) {
        alert("Error: " + message);
    }

    private void alert(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        bld.create().show();
    }

    // Enables or disables the "please wait" screen.
    void setWaitScreen(boolean set) {
        findViewById(R.id.screen_main).setVisibility(set ? View.GONE : View.VISIBLE);
        findViewById(R.id.screen_wait).setVisibility(set ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mHelper == null) return;

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.purchase_coins, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_help) {
            WhatAmIRiddlesUtil.help(this);
            return true;
        }
        if (id == R.id.action_rate) {
            WhatAmIRiddlesUtil.rateApp(this);
            return true;
        }
        if (id == R.id.action_share) {
            WhatAmIRiddlesUtil.shareApp(this);
            return true;
        }
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (id == R.id.action_remove_ads) {
            boolean areAdsRemoved = WhatAmIRiddlesUtil.areAdsRemoved(getApplicationContext());
            if (!areAdsRemoved) {
                setWaitScreen(true);
                if (mHelper != null) {
                    mHelper.launchPurchaseFlow(PurchaseCoinsActivity.this, REMOVE_ADS, 1, mPurchaseFinishedListener, REMOVE_ADS);
                }
            }
            else {
                showOkDialog("No Ads", "Ads are already removed. Thank you!");
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showOkDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_alert))
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //simply return
                    }
                }).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
    }
}
