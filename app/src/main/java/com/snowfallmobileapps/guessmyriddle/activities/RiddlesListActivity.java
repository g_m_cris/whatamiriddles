package com.snowfallmobileapps.guessmyriddle.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.snowfallmobileapps.guessmyriddle.R;
import com.snowfallmobileapps.guessmyriddle.adapters.RiddleAdapter;
import com.snowfallmobileapps.guessmyriddle.models.Levels;
import com.snowfallmobileapps.guessmyriddle.models.Riddle;
import com.snowfallmobileapps.guessmyriddle.util.WhatAmIRiddlesUtil;

import java.util.List;

public class RiddlesListActivity extends ActionBarActivity {

    public static final String SELECTED_RIDDLE = "SelectedRiddle";
    private List<Riddle> riddlesList;
    private String level;

    private RiddleAdapter riddlesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riddles_list);

        level = getIntent().getExtras().getString("level");
        if (level.equals(Levels.LEVEL1)) {
            riddlesList = Levels.getLevel1();
        }
        else if (level.equals(Levels.LEVEL2)) {
            riddlesList = Levels.getLevel2();
        }
        else if (level.equals(Levels.LEVEL3)) {
            riddlesList = Levels.getLevel3();
        }
        else {
            riddlesList = Levels.getLevel4();
        }

        ListView listview = (ListView) findViewById(R.id.riddlesListView);
        riddlesAdapter = new RiddleAdapter(this, getLayoutInflater(), riddlesList, level);
        listview.setAdapter(riddlesAdapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Intent intent = new Intent(RiddlesListActivity.this, RiddleActivity.class);
                intent.putExtra("level", level);
                intent.putExtra(SELECTED_RIDDLE, riddlesList.get(position));
                startActivity(intent);
            }
        });

        //admob
        /*
        AdView adView = (AdView) this.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(getString(R.string.admob_test_devices)).build();
        adView.loadAd(adRequest);
        */
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        riddlesAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.riddles_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_help) {
            WhatAmIRiddlesUtil.help(this);
            return true;
        }
        if (id == R.id.action_rate) {
            WhatAmIRiddlesUtil.rateApp(this);
            return true;
        }
        if (id == R.id.action_share) {
            WhatAmIRiddlesUtil.shareApp(this);
            return true;
        }
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
