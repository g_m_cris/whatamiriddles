/**
 * 
 */
package com.snowfallmobileapps.guessmyriddle.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.snowfallmobileapps.guessmyriddle.R;
import com.snowfallmobileapps.guessmyriddle.adapters.HelpPagerAdapter;
import com.snowfallmobileapps.guessmyriddle.util.WhatAmIRiddlesUtil;
import com.viewpagerindicator.CirclePageIndicator;

/**
 * @author mcgheorghisan
 *
 */
public class HelpActivity extends ActionBarActivity {

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.help_activity_layout);

        // help view
        HelpPagerAdapter helpAdapter = new HelpPagerAdapter();
        ViewPager helpPagerView = (ViewPager) findViewById(R.id.helpPagerView);
        helpPagerView.setAdapter(helpAdapter);
        helpPagerView.setCurrentItem(0);

        CirclePageIndicator circleIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        circleIndicator.setViewPager(helpPagerView);
        circleIndicator.setCurrentItem(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.help, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_rate) {
            WhatAmIRiddlesUtil.rateApp(this);
            return true;
        }
        if (id == R.id.action_share) {
            WhatAmIRiddlesUtil.shareApp(this);
            return true;
        }
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
