package com.snowfallmobileapps.guessmyriddle.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.appnext.appnextsdk.Appnext;
import com.snowfallmobileapps.guessmyriddle.R;
import com.snowfallmobileapps.guessmyriddle.models.Levels;
import com.snowfallmobileapps.guessmyriddle.models.Riddle;
import com.snowfallmobileapps.guessmyriddle.util.WhatAmIRiddlesUtil;

import java.util.List;

public class PuzzleActivity extends ActionBarActivity {

    List<Riddle> riddlesList;
    private Appnext appnext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puzzle);

        String level = getIntent().getExtras().getString("level");
        if (level.equals(Levels.LEVEL1)) {
            riddlesList = Levels.getLevel1();
        }
        else if (level.equals(Levels.LEVEL2)) {
            riddlesList = Levels.getLevel2();
        }
        else if (level.equals(Levels.LEVEL3)) {
            riddlesList = Levels.getLevel3();
        }
        else {
            riddlesList = Levels.getLevel4();
        }

        configureImageView(R.id.puzzle_image11, 0);
        configureImageView(R.id.puzzle_image12, 1);
        configureImageView(R.id.puzzle_image13, 2);

        configureImageView(R.id.puzzle_image21, 3);
        configureImageView(R.id.puzzle_image22, 4);
        configureImageView(R.id.puzzle_image23, 5);

        configureImageView(R.id.puzzle_image31, 6);
        configureImageView(R.id.puzzle_image32, 7);
        configureImageView(R.id.puzzle_image33, 8);

        configureImageView(R.id.puzzle_image41, 9);
        configureImageView(R.id.puzzle_image42, 10);
        configureImageView(R.id.puzzle_image43, 11);

        int countSolved = getIntent().getExtras().getInt("countSolved");

        boolean areAdsRemoved = WhatAmIRiddlesUtil.areAdsRemoved(getApplicationContext());
        if (!areAdsRemoved) {
            //app next ads
            appnext = new Appnext(this);
            appnext.setAppID(getResources().getString(R.string.placementId)); // Set your AppID

            if (countSolved == 3 || countSolved == 5 || countSolved == 7 || countSolved == 9 || countSolved == 11) {
                appnext.showBubble();
            }
        }
    }

    private void configureImageView(int imageId, int position) {
        ImageView imageView = (ImageView) findViewById(imageId);

        Riddle riddle = riddlesList.get(position);
        riddle.retrieveRiddleCompletion(getApplicationContext());

        if (riddle.completed.equals("Y")) {
            imageView.setImageDrawable(getResources().getDrawable(riddle.puzzlePiece));
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.puzzle, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_help) {
            WhatAmIRiddlesUtil.help(this);
            return true;
        }
        if (id == R.id.action_rate) {
            WhatAmIRiddlesUtil.rateApp(this);
            return true;
        }
        if (id == R.id.action_share) {
            WhatAmIRiddlesUtil.shareApp(this);
            return true;
        }
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
