package com.snowfallmobileapps.guessmyriddle.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.snowfallmobileapps.guessmyriddle.R;
import com.snowfallmobileapps.guessmyriddle.models.Levels;
import com.snowfallmobileapps.guessmyriddle.models.Money;
import com.snowfallmobileapps.guessmyriddle.models.Riddle;
import com.snowfallmobileapps.guessmyriddle.util.WhatAmIRiddlesUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import static com.snowfallmobileapps.guessmyriddle.models.Money.MONEY_RIDDLE_GAIN;


public class RiddleActivity extends ActionBarActivity {

    public final static String LETTERS = "Letters";
    public final static String LETTERS_COLORS = "LettersColors";
    public final static String HINT_BOUGHT = "HintBought";
    public final static String COLOR_GREEN = "#04B431";

    private Riddle riddle;
    private LinearLayout answerLayout;
    private TextView bagOfMoneyTextView;
    private ImageButton hintImageButton;

    private List<EditText> answerLettersList = new ArrayList<EditText>();
    private String[] letters;
    private int[] letterColor;

    private String level;
    private boolean isHintBought = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_riddle);

        level = getIntent().getExtras().getString("level");
        riddle = getIntent().getParcelableExtra(RiddlesListActivity.SELECTED_RIDDLE);
        letters = new String[riddle.answer.length()];
        letterColor = new int[riddle.answer.length()];

        String hintKey = riddle.id + "_hint";
        String lettersPositionsKey = riddle.id + "_letters";
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        isHintBought = preferences.getBoolean(hintKey, false);
        Set letterBought = preferences.getStringSet(lettersPositionsKey, null);

        for (int i = 0; i < riddle.answer.length(); i++) {
            letters[i] = "";
            letterColor[i] = Color.BLACK;
            if (letterBought != null && letterBought.size() > 0 && letterBought.contains("" + i)) {
                letters[i] = "" + riddle.answer.charAt(i);
                letterColor[i] = Color.parseColor(COLOR_GREEN);
            }
        }

        ImageButton bagOfMoney = (ImageButton) findViewById(R.id.bagOfMoney);
        bagOfMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(RiddleActivity.this, PurchaseCoinsActivity.class);
                startActivity(newIntent);
            }
        });

        ImageButton shareRiddle = (ImageButton) findViewById(R.id.shareImageView);
        shareRiddle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareRiddle();
            }
        });

        TextView riddleTextView = (TextView) findViewById(R.id.riddleTextView);
        riddleTextView.setText(riddle.text);
        riddleTextView.setMovementMethod(new ScrollingMovementMethod());

        answerLayout = (LinearLayout) findViewById(R.id.answerLayout);

        bagOfMoneyTextView = (TextView) findViewById(R.id.bagOfMoneyTextView);
        updateBagOfMoneyTextView();

        hintImageButton = (ImageButton) findViewById(R.id.hintImageView);
        hintImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isHintBought) {
                    int bagOfMoney = Money.getBagOfMoney(getApplicationContext());
                    if (bagOfMoney >= Money.MONEY_HINT_COST) {
                        showHintDialog();
                    } else {
                        showHintNoMoneyDialog();
                    }
                }
                else{
                    showOkDialog("Hint", riddle.hint);
                }
            }
        });

        LinearLayout letterLayout = (LinearLayout) findViewById(R.id.letterLayout);
        ImageButton letterImageButton = (ImageButton) findViewById(R.id.letterImageView);
        letterImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int bagOfMoney = Money.getBagOfMoney(getApplicationContext());
                if (bagOfMoney >= Money.MONEY_LETTER_COST) {
                    showLetterDialog();
                }
                else {
                    showLetterNoMoneyDialog();
                }
            }
        });

        ImageButton puzzleImageButton = (ImageButton) findViewById(R.id.puzzleImageView);
        puzzleImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(RiddleActivity.this, PuzzleActivity.class);
                newIntent.putExtra("level", level);
                startActivity(newIntent);
            }
        });

        ImageButton guess = (ImageButton) findViewById(R.id.guess);
        guess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateAnswer();
            }
        });
        TextView needHelpTextView = (TextView) findViewById(R.id.needHelpTextView);
        TextView instructionsTextView = (TextView) findViewById(R.id.instructionsTextView);

        if (riddle.completed.equals("Y")) {
            guess.setVisibility(View.GONE);
            hintImageButton.setVisibility(View.INVISIBLE);
            letterLayout.setVisibility(View.INVISIBLE);
            needHelpTextView.setVisibility(View.INVISIBLE);
            instructionsTextView.setVisibility(View.GONE);
            puzzleImageButton.setVisibility(View.VISIBLE);
        }
        else {
            guess.setVisibility(View.VISIBLE);
            hintImageButton.setVisibility(View.VISIBLE);
            letterLayout.setVisibility(View.VISIBLE);
            needHelpTextView.setVisibility(View.VISIBLE);
            instructionsTextView.setVisibility(View.VISIBLE);
            puzzleImageButton.setVisibility(View.GONE);
        }

        buildAnswerBoxes();

        //admob
        /*
        AdView adView = (AdView) this.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(getString(R.string.admob_test_devices)).build();
        adView.loadAd(adRequest);
        */
    }

    private void shareRiddle()
    {
        // Open all sharing option for user
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Guess My Riddle!");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,
                riddle.text + "\n \n " +
                        "https://play.google.com/store/apps/details?id=com.snowfallmobileapps.guess_my_riddle");
        sharingIntent.putExtra(Intent.EXTRA_TITLE, "Guess My Riddle!");
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    private void updateBagOfMoneyTextView () {
        bagOfMoneyTextView.setText("" + Money.getBagOfMoney(getApplicationContext()));
    }

    private void showHintDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Confirmation")
                .setMessage(getResources().getString(R.string.activity_riddle_hint_confirmation))
                .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_alert))
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Money.decreaseBagOfMoney(Money.MONEY_HINT_COST, getApplicationContext());
                                updateBagOfMoneyTextView();
                                showOkDialog("Hint", riddle.hint);
                                isHintBought = true;
                                //hintImageButton.setVisibility(View.INVISIBLE);
                            }
                        })
                .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Do Nothing
                    }
                }).show();
    }

    private void showHintNoMoneyDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Confirmation")
                .setMessage(getResources().getString(R.string.activity_riddle_hint_not_enough_coins))
                .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_alert))
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent newIntent = new Intent(RiddleActivity.this, PurchaseCoinsActivity.class);
                        startActivity(newIntent);
                    }
                })
                .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Do Nothing
                    }
                }).show();
    }

    private void showLetterDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Confirmation")
                .setMessage(getResources().getString(R.string.activity_riddle_letter_confirmation))
                .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_alert))
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Money.decreaseBagOfMoney(Money.MONEY_LETTER_COST, getApplicationContext());
                        updateBagOfMoneyTextView();
                        //show the letter
                        fillInTheLetter();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Do Nothing
                    }
                }).show();
    }

    private void showLetterNoMoneyDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Confirmation")
                .setMessage(getResources().getString(R.string.activity_riddle_letter_not_enough_coins))
                .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_alert))
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    Intent newIntent = new Intent(RiddleActivity.this, PurchaseCoinsActivity.class);
                    startActivity(newIntent);
                    }
                })
                .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Do Nothing
                    }
                }).show();
    }

    private void fillInTheLetter() {
        String answer = buildTheAnswer();
        if (riddle.answer.equals(answer)) {
            showOkDialog("A Letter", "Your answer might be correct. Go ahead and try the guess button!");
            return;
        }
        String aLetterCandidate = "";
        String correspondingLetterInTheAnswer = "";
        int aLetterCandidatePosition = 0;
        char[] riddleAnswerLetters = riddle.answer.toCharArray();
        do {
            aLetterCandidatePosition = getRandomLetterPosition();
            aLetterCandidate = answerLettersList.get(aLetterCandidatePosition).getText().toString();
            correspondingLetterInTheAnswer = "" + riddleAnswerLetters[aLetterCandidatePosition];
        }
        while (aLetterCandidate.equals(correspondingLetterInTheAnswer));

        //display the letter and a message
        answerLettersList.get(aLetterCandidatePosition).setText(correspondingLetterInTheAnswer);
        answerLettersList.get(aLetterCandidatePosition).setTextColor(Color.parseColor(COLOR_GREEN));
        answerLettersList.get(aLetterCandidatePosition).setEnabled(false);
        letterColor[aLetterCandidatePosition] = Color.parseColor(COLOR_GREEN);
    }


    private int getRandomLetterPosition() {
        //generate random number between min and max-1
        int min = 0;
        int max = riddle.answer.length();

        Random r = new Random();
        int randomLetterPosition = r.nextInt(max - min) + min;

        return randomLetterPosition;
    }

    private void buildAnswerBoxes() {
        String riddleAnswer = riddle.answer;
        for (int i = 0; i < riddleAnswer.length(); i++) {
            char c = riddleAnswer.charAt(i);
            final EditText editText = (EditText) getLayoutInflater().inflate(R.layout.element_riddle_answer, null);
            //editText.setLayoutParams(new LinearLayout.LayoutParams(70, 70));
            editText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
            editText.setFocusable(true);
            editText.setBackgroundColor(Levels.colorHexCodesAnswer[i]);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    View view;
                    if (s.length() == 1) {
                        //when adding a letter
                        view = editText.focusSearch(View.FOCUS_RIGHT);
                        if (view instanceof EditText) {
                            view.requestFocus();
                        }
                    } else {
                        //when deleting
                        view = editText.focusSearch(View.FOCUS_LEFT);
                        if (view instanceof EditText) {
                            view.requestFocus();
                        }
                    }
                }
            });

            if (riddle.completed.equals("Y")) {
                editText.setText("" + c);
                editText.setEnabled(false);
            }
            else {
                editText.setText("" + letters[i]);
                editText.setTextColor(letterColor[i]);
                if (letterColor[i] == Color.parseColor(COLOR_GREEN)) {
                    editText.setEnabled(false);
                }
            }
            answerLettersList.add(editText);
            answerLayout.addView(editText);
        }
    }

    private String buildTheAnswer() {
        String answer = "";
        for (EditText editText : answerLettersList) {
            answer = answer +  editText.getText().toString().toLowerCase();
        }

        return answer;
    }

    private void validateAnswer() {
        String answer = buildTheAnswer();

        if (answer.equals(riddle.answer)) {
            //increase the bag of money
            Money.increaseBagOfMoney(MONEY_RIDDLE_GAIN, getApplicationContext());

            //mark riddle as completed
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = preferences.edit();
            riddle.completed = "Y";
            editor.putString(riddle.id, riddle.completed);

            //clear pref
            String hintKey = riddle.id + "_hint";
            String lettersPositionsKey = riddle.id + "_letters";
            editor.remove(hintKey);
            editor.remove(lettersPositionsKey);

            editor.apply();

            //display the congradulations screen
            Intent newIntent = new Intent(RiddleActivity.this, RiddleGuessedActivity.class);
            newIntent.putExtra("level", level);
            startActivity(newIntent);
            finish();
        }
        else {
            showOkDialog("Wrong answer", getResources().getString(R.string.activity_riddle_answerWrong));
        }
    }

    private void showOkDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_alert))
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //simply return
                    }
                }).show();
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        for (int i = 0; i < answerLettersList.size(); i++) {
            letters[i] = answerLettersList.get(i).getText().toString();
        }
        state.putStringArray(LETTERS, letters);
        state.putIntArray(LETTERS_COLORS, letterColor);
        //save hint state
        state.putBoolean(HINT_BOUGHT, isHintBought);
    }

    @Override
    public void onBackPressed() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        //store the updated value
        SharedPreferences.Editor editor = preferences.edit();
        String hintKey = riddle.id + "_hint";
        String lettersPositionsKey = riddle.id + "_letters";
        Set<String> lettersBought = new HashSet<String>();
        for (int i = 0; i < letterColor.length; i++) {
            if (letterColor[i] == Color.parseColor(COLOR_GREEN)) {
                lettersBought.add("" + i);
            }
        }

        editor.putBoolean(hintKey, isHintBought);
        editor.putStringSet(lettersPositionsKey, lettersBought);

        editor.apply();

        super.onBackPressed();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        //restore the letters entered by the user previously
        if (savedInstanceState != null) {
            if (savedInstanceState.getSerializable(LETTERS) != null) {
                letters = savedInstanceState.getStringArray(LETTERS);
                letterColor = savedInstanceState.getIntArray(LETTERS_COLORS);
                for (int i = 0; i < answerLettersList.size(); i++) {
                    answerLettersList.get(i).setText(letters[i]);
                    answerLettersList.get(i).setTextColor(letterColor[i]);
                    if (letterColor[i] == Color.parseColor(COLOR_GREEN)) {
                        answerLettersList.get(i).setEnabled(false);
                    }
                }
            }
            if (savedInstanceState.getSerializable(HINT_BOUGHT) != null) {
                isHintBought = savedInstanceState.getBoolean(HINT_BOUGHT);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.riddle, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_help) {
            WhatAmIRiddlesUtil.help(this);
            return true;
        }
        if (id == R.id.action_rate) {
            WhatAmIRiddlesUtil.rateApp(this);
            return true;
        }
        if (id == R.id.action_share) {
            WhatAmIRiddlesUtil.shareApp(this);
            return true;
        }
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
