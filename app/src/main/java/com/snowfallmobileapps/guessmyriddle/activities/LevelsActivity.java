package com.snowfallmobileapps.guessmyriddle.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.snowfallmobileapps.guessmyriddle.R;
import com.snowfallmobileapps.guessmyriddle.models.Levels;
import com.snowfallmobileapps.guessmyriddle.util.WhatAmIRiddlesUtil;


public class LevelsActivity extends ActionBarActivity {

    private LinearLayout level1;
    private LinearLayout level2;
    private LinearLayout level3;
    private LinearLayout level4;

    private int completionLevel1;
    private int completionLevel2;
    private int completionLevel3;
    private int completionLevel4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_levels);

        level1 = (LinearLayout) findViewById(R.id.level1);
        level2 = (LinearLayout) findViewById(R.id.level2);
        level3 = (LinearLayout) findViewById(R.id.level3);
        level4 = (LinearLayout) findViewById(R.id.level4);

        completionLevel1 = configureLevelLayout(level1, 0, 0);
        completionLevel2 = configureLevelLayout(level2, 1, completionLevel1);
        completionLevel3 = configureLevelLayout(level3, 2, completionLevel2);
        completionLevel4 = configureLevelLayout(level4, 3, completionLevel4);

        level1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(LevelsActivity.this, RiddlesListActivity.class);
                String level = "LEVEL 1";
                newIntent.putExtra("level", level);
                startActivity(newIntent);
            }
        });

        level2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (completionLevel1 == 12) {
                    Intent newIntent = new Intent(LevelsActivity.this, RiddlesListActivity.class);
                    String level = "LEVEL 2";
                    newIntent.putExtra("level", level);
                    startActivity(newIntent);
                }
                else {
                    showOkDialog("Level locked", getResources().getString(R.string.activity_levels_locked_level_message));
                }
            }
        });

        level3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (completionLevel2 == 12) {
                    Intent newIntent = new Intent(LevelsActivity.this, RiddlesListActivity.class);
                    String level = "LEVEL 3";
                    newIntent.putExtra("level", level);
                    startActivity(newIntent);
                }
                else {
                    showOkDialog("Level locked", getResources().getString(R.string.activity_levels_locked_level_message));
                }
            }
        });

        level4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (completionLevel3 == 12) {
                    Intent newIntent = new Intent(LevelsActivity.this, RiddlesListActivity.class);
                    String level = "LEVEL 4";
                    newIntent.putExtra("level", level);
                    startActivity(newIntent);
                }
                else {
                    showOkDialog("Level locked", getResources().getString(R.string.activity_levels_locked_level_message));
                }
            }
        });
    }

    private int configureLevelLayout(LinearLayout convertView, final int position, final int previousCompletionLevel) {

        //set progress bar completion level
        int completionLevel = Levels.getLevelCompletionProgress(position + 1, getApplicationContext());
        ProgressBar prgBarLev = (ProgressBar) convertView.findViewById(R.id.progressBarLevel);
        prgBarLev.setProgress(completionLevel);
        //set completion level text
        TextView levelRiddlesCompletedTextView = (TextView)convertView.findViewById(R.id.levelRiddlesCompletedTextView);
        levelRiddlesCompletedTextView.setText(completionLevel + "/12");

        TextView levelNumberTextView = (TextView)convertView.findViewById(R.id.levelNumberTextView);
        levelNumberTextView.setText("LEVEL " + (position + 1));
        //set the lock
        ImageView lockImageView = (ImageView) convertView.findViewById(R.id.lockImageView);
        if (completionLevel == 12) {
            lockImageView.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.transparentgreencheckmark));
        }
        else {
            if (position > 0) {
                if (previousCompletionLevel == 12) {
                    lockImageView.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.open_lock));
                }
                else {
                    lockImageView.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.closed_lock));
                }
            }
        }

        return completionLevel;
    }

    private void showOkDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_alert))
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //simply return
                    }
                }).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.levels, menu);
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        completionLevel1 = configureLevelLayout(level1, 0, 0);
        completionLevel2 = configureLevelLayout(level2, 1, completionLevel1);
        completionLevel3 = configureLevelLayout(level3, 2, completionLevel2);
        completionLevel4 = configureLevelLayout(level4, 3, completionLevel4);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_help) {
            WhatAmIRiddlesUtil.help(this);
            return true;
        }
        if (id == R.id.action_rate) {
            WhatAmIRiddlesUtil.rateApp(this);
            return true;
        }
        if (id == R.id.action_share) {
            WhatAmIRiddlesUtil.shareApp(this);
            return true;
        }
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
