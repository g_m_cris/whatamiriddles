package com.snowfallmobileapps.guessmyriddle.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.appnext.appnextsdk.Appnext;
import com.snowfallmobileapps.guessmyriddle.R;
import com.snowfallmobileapps.guessmyriddle.models.Levels;
import com.snowfallmobileapps.guessmyriddle.models.Money;
import com.snowfallmobileapps.guessmyriddle.util.WhatAmIRiddlesUtil;

import static com.snowfallmobileapps.guessmyriddle.models.Levels.getLevelCompletionProgress;

public class RiddleGuessedActivity extends ActionBarActivity {

    private String level;
    private Appnext appnext;
    private int countSolved;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riddle_guessed);

        level = getIntent().getExtras().getString("level");
        int levelNumber = 0;
        if (level.equals(Levels.LEVEL1)) {
            levelNumber = 1;
        }
        else if (level.equals(Levels.LEVEL2)) {
            levelNumber = 2;
        }
        else if (level.equals(Levels.LEVEL3)) {
            levelNumber = 3;
        }
        else {
            levelNumber = 4;
        }
        countSolved = getLevelCompletionProgress(levelNumber, getApplicationContext());
        if (countSolved == 12) {
            if (levelNumber == 4) {
                showOkDialog("Game Completed!", "Congratulations! You've finished all the levels of the riddle game!!! \n \n" +
                        "If you enjoyed it, continue the adventure with Guess My Riddle 2!");
            }
            else {
                showOkDialogExtra("Level Completed!", "Congratulations! You've finished level " + levelNumber + "!\n" +
                        "You gained a bonus of "
                        + Money.MONEY_LEVEL_GAIN + " coins and unlocked level " + (levelNumber + 1) + "!");
            }
        }


        ImageButton puzzleButton = (ImageButton) findViewById(R.id.puzzleButton);
        puzzleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(RiddleGuessedActivity.this, PuzzleActivity.class);
                newIntent.putExtra("level", level);
                newIntent.putExtra("countSolved", countSolved);
                startActivity(newIntent);
                finish();
            }
        });

        boolean areAdsRemoved = WhatAmIRiddlesUtil.areAdsRemoved(getApplicationContext());
        if (!areAdsRemoved) {
            //app next ads
            appnext = new Appnext(this);
            appnext.setAppID(getResources().getString(R.string.placementId)); // Set your AppID
            appnext.addMoreAppsRight(getResources().getString(R.string.placementId));

            if (countSolved == 2 || countSolved == 4 || countSolved == 6 || countSolved == 8 || countSolved == 10) {
                appnext.showBubble();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(appnext.isBubbleVisible()){
            appnext.hideBubble();
        }
        else{
            super.onBackPressed();
        }
    }



    private void showOkDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_alert))
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //simply return
                    }
                }).show();
    }

    private void showOkDialogExtra(String title, String message) {
        Resources res = RiddleGuessedActivity.this.getResources();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(message);
        stringBuilder.append("\n \n");
        stringBuilder.append(res.getString(R.string.support_the_developers));
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.parseColor("#33b5e5"));
        CharSequence spanedString = WhatAmIRiddlesUtil.setSpanBetweenTokens(stringBuilder, "##", colorSpan);
        colorSpan = new ForegroundColorSpan(Color.parseColor("#33b5e5"));
        spanedString = WhatAmIRiddlesUtil.setSpanBetweenTokens(spanedString, "##", colorSpan);

        AlertDialog.Builder builder = new AlertDialog.Builder(RiddleGuessedActivity.this);
        // add the second button only if there was no internet connection
        builder.setTitle(title);
        builder.setMessage(spanedString);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    dialog.dismiss();
                    dialog = null;
                } catch (Exception e) {
                    // nothing
                }
            }
        });
        builder.setCancelable(false);
        builder.create().show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.riddle_guessed, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_help) {
            WhatAmIRiddlesUtil.help(this);
            return true;
        }
        if (id == R.id.action_rate) {
            WhatAmIRiddlesUtil.rateApp(this);
            return true;
        }
        if (id == R.id.action_share) {
            WhatAmIRiddlesUtil.shareApp(this);
            return true;
        }
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
