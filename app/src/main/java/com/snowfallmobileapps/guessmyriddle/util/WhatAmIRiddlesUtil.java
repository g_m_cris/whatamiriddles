package com.snowfallmobileapps.guessmyriddle.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.text.SpannableStringBuilder;
import android.text.style.CharacterStyle;

import com.snowfallmobileapps.guessmyriddle.activities.HelpActivity;

/**
 * Created by CGheorghisan on 9/3/2014.
 */
public class WhatAmIRiddlesUtil {

    public static final String ARE_ADS_REMOVED = "AreAdsRemoved";

    public static void help(Activity activity) {
        Intent intent = new Intent(activity, HelpActivity.class);
        activity.startActivity(intent);
    }

    public static void rateApp(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=com.snowfallmobileapps.guess_my_riddle"));
        activity.startActivity(intent);
    }

    public static void shareApp(Activity activity)
    {
        // Open all sharing option for user
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Try Guess My Riddle!");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.snowfallmobileapps.guess_my_riddle");
        sharingIntent.putExtra(Intent.EXTRA_TITLE, "Try Guess My Riddle!");
        activity.startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    /**
     * Given either a Spannable String or a regular String and a token, apply the given CharacterStyle to the span between the tokens, and also remove tokens.
     * <p>
     * For example, {@code setSpanBetweenTokens("Hello ##world##!", "##",
     * new ForegroundColorSpan(0xFFFF0000));} will return a CharSequence {@code "Hello world!"} with {@code world} in red.
     *
     * @param text
     *            The text, with the tokens, to adjust.
     * @param token
     *            The token string; there should be at least two instances of token in text.
     * @param cs
     *            The style to apply to the CharSequence. WARNING: You cannot send the same two instances of this parameter, otherwise the second call will remove the original
     *            span.
     * @return A Spannable CharSequence with the new style applied.
     *
     * @see http://developer.android.com/reference/android/text/style/CharacterStyle.html
     */
    public static CharSequence setSpanBetweenTokens(CharSequence text, String token, CharacterStyle... cs) {
        // Start and end refer to the points where the span will apply
        int tokenLen = token.length();
        int start = text.toString().indexOf(token) + tokenLen;
        int end = text.toString().indexOf(token, start);

        if (start > -1 && end > -1) {
            // Copy the spannable string to a mutable spannable string
            SpannableStringBuilder ssb = new SpannableStringBuilder(text);
            for (CharacterStyle c : cs)
                ssb.setSpan(c, start, end, 0);

            // Delete the tokens before and after the span
            ssb.delete(end, end + tokenLen);
            ssb.delete(start - tokenLen, start);

            text = ssb;
        }

        return text;
    }

    public static boolean areAdsRemoved(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean areAdsRemove = preferences.getBoolean(ARE_ADS_REMOVED, false);

        return  areAdsRemove;
    }

    public static void setAdsRemoved(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        //store the updated value
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(ARE_ADS_REMOVED, true);
        editor.apply();
    }
}
