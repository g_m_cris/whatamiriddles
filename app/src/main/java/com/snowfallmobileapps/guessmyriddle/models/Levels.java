package com.snowfallmobileapps.guessmyriddle.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;

import com.snowfallmobileapps.guessmyriddle.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CGheorghisan on 8/28/2014.
 */
public class Levels {
    public final static String LEVEL1 = "LEVEL 1";
    public final static String LEVEL2 = "LEVEL 2";
    public final static String LEVEL3 = "LEVEL 3";
    public final static String LEVEL4 = "LEVEL 4";

    private static List<Riddle> level1 = new ArrayList<Riddle>();
    private static List<Riddle> level2 = new ArrayList<Riddle>();
    private static List<Riddle> level3 = new ArrayList<Riddle>();
    private static List<Riddle> level4 = new ArrayList<Riddle>();

    public static int[] colorHexCodesAnswer = {Color.parseColor("#CFFFFE"), Color.parseColor("#F8FFCF"), Color.parseColor("#CFF0FF"),
            Color.parseColor("#CFFFD6"), Color.parseColor("#FFCFE0"), Color.parseColor("#CFE0FF"), Color.parseColor("#DECFFF"),
            Color.parseColor("#FFCFCF"), Color.parseColor("#CFFFDE"), Color.parseColor("#FECFFF"), Color.parseColor("#E6CFFF"),
            Color.parseColor("#CFFFD6"), Color.parseColor("#FFF6CF"), Color.parseColor("#D6CFFF"), Color.parseColor("#EECFFF"),
            Color.parseColor("#CFFFEE")};

    public static List<Riddle> getLevel1() {
        if (level1.size() == 0) {
            createLevel1Riddles();
        }

        return level1;
    }

    public static List<Riddle> getLevel2() {
        if (level2.size() == 0) {
            createLevel2Riddles();
        }

        return level2;
    }

    public static List<Riddle> getLevel3() {
        if (level3.size() == 0) {
            createLevel3Riddles();
        }

        return level3;
    }

    public static List<Riddle> getLevel4() {
        if (level4.size() == 0) {
            createLevel4Riddles();
        }

        return level4;
    }
    private static void createLevel1Riddles() {
        Riddle r1 = new Riddle("r1",
                "Underneath the bright Sun's rays \n" +
                        "Over fields a flower strays",
                "cloud",
                "It's white",
                R.drawable.level1_1_1);

        Riddle r2 = new Riddle("r2",
                "I am sweet for you to pour,\n" +
                        "When it's hot, I become sour",
                "milk",
                "Helps with sleep",
                R.drawable.level1_1_2);

        Riddle r3 = new Riddle("r3",
                "Thrown up I'm white\n" +
                        "Thrown down I'm yellow",
                "egg",
                "I am very fragile",
                R.drawable.level1_1_3);

        Riddle r4 = new Riddle("r4",
                "Show my back to any king\n" +
                        "Though no disrespect I bring",
                "driver",
                "I lead the way",
                R.drawable.level1_2_1);

        Riddle r5 = new Riddle("r5",
                "What water is there in the world\n" +
                        "Surrounded not by sand?",
                "tear",
                "It's salty",
                R.drawable.level1_2_2);

        Riddle r6 = new Riddle("r6",
                "Pretty girl of orange dress\n" +
                        "I once tried to thy undress\n" +
                        "But no joy was I to find\n" +
                        "In tears I was left behind",
                "onion",
                "Stings the tongue and stings the eyes",
                R.drawable.level1_2_3);

        Riddle r7 = new Riddle("r7",
                "Soulless it feels,\n" +
                        "Many souls it steals",
                "shotgun",
                "Made of metal",
                R.drawable.level1_3_1);

        Riddle r8 = new Riddle("r8",
                "Though not asked\n" +
                        "I will reply\n" +
                        "You won't find me\n" +
                        "If you try",
                "echo",
                "It repeats what you say",
                R.drawable.level1_3_2);

        Riddle r9 = new Riddle("r9",
                "I can fly high in the sky\n" +
                        "You won't see me passing by",
                "wind",
                "I turn umbrellas inside out",
                R.drawable.level1_3_3);

        Riddle r10 = new Riddle("r10",
                "After rain we bask with pride\n" +
                        "Tiny shading we provide",
                "mushrooms",
                "We grow, but we're not flowers",
                R.drawable.level1_4_1);

        Riddle r11 = new Riddle("r11",
                "Little prince of cape so white\n" +
                        "Through the snow grows young and bright",
                "snowdrop",
                "First of spring",
                R.drawable.level1_4_2);

        Riddle r12 = new Riddle("r12",
                "Towards the Sun it springs each morning\n" +
                        "Filling the air with joy and singing",
                "lark",
                "I know many songs",
                R.drawable.level1_4_3);

        level1.add(r1);
        level1.add(r2);
        level1.add(r3);
        level1.add(r4);
        level1.add(r5);
        level1.add(r6);
        level1.add(r7);
        level1.add(r8);
        level1.add(r9);
        level1.add(r10);
        level1.add(r11);
        level1.add(r12);
    }

    private static void createLevel2Riddles() {
        Riddle r13 = new Riddle("r13",
                "In one house four brothers rest\n" +
                        "Through the rain their shelter's best",
                "walnut",
                "Has a hard shell",
                R.drawable.level2_1_1);

        Riddle r14 = new Riddle("r14",
                "Hisses, though it's not a snake\n" +
                        "Holds water, it's not a lake",
                "kettle",
                "Used in the kitchen",
                R.drawable.level2_1_2);

        Riddle r15 = new Riddle("r15",
                "Has a mouth and has two ears\n" +
                        "Neither speaks, nor does it hear",
                "statue",
                "Made of stone or metal",
                R.drawable.level2_1_3);

        Riddle r16 = new Riddle("r16",
                "Tree of four branches,\n" +
                        "One branch blossoms, the second bears fruits,\n" +
                        "The third withers and the fourth is bare",
                "year",
                "It gets repeated over and over again",
                R.drawable.level2_2_1);

        Riddle r17 = new Riddle("r17",
                "Fortress unlocked\n" +
                        "Awaits your visit\n" +
                        "Take all you want\n" +
                        "There is no limit",
                "knowledge",
                "Opens your mind",
                R.drawable.level2_2_2);

        Riddle r18 = new Riddle("r18",
                "Say nothing and you call my name",
                "silence",
                "I am very, very quiet",
                R.drawable.level2_2_3);

        Riddle r19 = new Riddle("r19",
                "Flaps its wings all the time,\n" +
                        "but never flies",
                "windmill",
                "Used to grind",
                R.drawable.level2_3_1);

        Riddle r20 = new Riddle("r20",
                "Keeps running,\n" +
                        "though it stands still",
                "clock",
                "It ticks",
                R.drawable.level2_3_2);

        Riddle r21 = new Riddle("r21",
                "Like a hen its chicks it gathers\n" +
                        "Every night\n" +
                        "And next day all of them it scatters\n" +
                        "Come daylight",
                "house",
                "You go back to it",
                R.drawable.level2_3_3);

        Riddle r22 = new Riddle("r22",
                "Who goes back and forth,\n" +
                        "but stays in place?",
                "door",
                "You find it in a house",
                R.drawable.level2_4_1);

        Riddle r23 = new Riddle("r23",
                "What lays a sturdy bridge\n" +
                        "above the waters overnight?",
                "winter",
                "It's cold",
                R.drawable.level2_4_2);

        Riddle r24 = new Riddle("r24",
                "How do you call the woman\n" +
                        "Who always knows where her husband is?",
                "widow",
                "She's alone",
                R.drawable.level2_4_3);

        level2.add(r13);
        level2.add(r14);
        level2.add(r15);
        level2.add(r16);
        level2.add(r17);
        level2.add(r18);
        level2.add(r19);
        level2.add(r20);
        level2.add(r21);
        level2.add(r22);
        level2.add(r23);
        level2.add(r24);
    }

    private static void createLevel3Riddles() {
        Riddle r25 = new Riddle("r25",
                "What is greatest than anything \n" +
                        "and above all?",
                "law",
                "It's a set of rules",
                R.drawable.level3_1_1);

        Riddle r26 = new Riddle("r26",
                "Some have it whole\n" +
                        "Some only half\n" +
                        "Some none at all",
                "family",
                "You don't get to choose it",
                R.drawable.level3_1_2);

        Riddle r27 = new Riddle("r27",
                "Can you think long and hard and say\n" +
                        "What’s the richest in the world today?",
                "land",
                "We live on it",
                R.drawable.level3_1_3);

        Riddle r28 = new Riddle("r28",
                "What can lay on water\n" +
                        "and not get wet?",
                "shadow",
                "Follows you around",
                R.drawable.level3_2_1);

        Riddle r29 = new Riddle("r29",
                "You can’t taste it, you can’t see it,\n" +
                        "But you’re always going to need it",
                "air",
                "Life depends on it",
                R.drawable.level3_2_2);

        Riddle r30 = new Riddle("r30",
                "Has no mouth, but still it chatters\n" +
                        "And sometimes it even stutters",
                "radio",
                "Boxy with dials",
                R.drawable.level3_2_3);

        Riddle r31 = new Riddle("r31",
                "On land I crawl and struggle,\n" +
                        "In water I'm a graceful marvel",
                "seal",
                "Has whiskers",
                R.drawable.level3_3_1);

        Riddle r32 = new Riddle("r32",
                "One thousand strings, one thousand ties,\n" +
                        "One thousand guessed, none got it right",
                "trawl",
                "Water goes through it",
                R.drawable.level3_3_2);

        Riddle r33 = new Riddle("r33",
                "The more you take, the more it grows.\n" +
                        "The more you put, the smaller it turns.",
                "pit",
                "It's dark",
                R.drawable.level3_3_3);

        Riddle r34 = new Riddle("r34",
                "Do you know which bird can fly\n" +
                        "Only when you have it tied?",
                "kite",
                "I have many colors and the kids love me",
                R.drawable.level3_4_1);

        Riddle r35 = new Riddle("r35",
                "I am black and I am small,\n" +
                        "If I jump you won't find me at all!",
                "flea",
                "You don't like me because I hurt you",
                R.drawable.level3_4_2);

        Riddle r36 = new Riddle("r36",
                "What is sweetest of them all,\n" +
                        "Sent away and back it crawls?",
                "sleep",
                "It's your best friend in the morning",
                R.drawable.level3_4_3);

        level3.add(r25);
        level3.add(r26);
        level3.add(r27);
        level3.add(r28);
        level3.add(r29);
        level3.add(r30);
        level3.add(r31);
        level3.add(r32);
        level3.add(r33);
        level3.add(r34);
        level3.add(r35);
        level3.add(r36);
    }

    private static void createLevel4Riddles() {
        Riddle r37 = new Riddle("r37",
                "Has big eyes\n" +
                        "And fast it flies\n" +
                        "Close to ponds and reeds it lies",
                "dragonfly",
                "Has four wings",
                R.drawable.level4_1_1);

        Riddle r38 = new Riddle("r38",
                "Dried mushroom top\n" +
                        "Hooked in a pin",
                "hat",
                "Used against Sun or rain",
                R.drawable.level4_1_2);

        Riddle r39 = new Riddle("r39",
                "From earth I am born,\n" +
                        "In water I die\n" +
                        "In air I dry\n" +
                        "And in fire I'm formed",
                "brick",
                "Used for building",
                R.drawable.level4_1_3);

        Riddle r40 = new Riddle("r40",
                "Though it only has one eye\n" +
                        "What it sees can't be denied",
                "camera",
                "Collects memories",
                R.drawable.level4_2_1);

        Riddle r41 = new Riddle("r41",
                "I have a beard,\n" +
                        "But I'm not a man",
                "goat",
                "I climb rocks",
                R.drawable.level4_2_2);

        Riddle r42 = new Riddle("r42",
                "I'm small and laborious\n" +
                        "But not as generous",
                "ant",
                "I live in groups",
                R.drawable.level4_2_3);

        Riddle r43 = new Riddle("r43",
                "Little dwarfs with lanterns\n" +
                        "Walk the wood’s pathways",
                "fireflies",
                "Tiny lights in the night",
                R.drawable.level4_3_1);

        Riddle r44 = new Riddle("r44",
                "I have scissors,\n" +
                        "But I don't tailor",
                "crab",
                "I like water",
                R.drawable.level4_3_2);

        Riddle r45 = new Riddle("r45",
                "Flatten balloon\n" +
                        "Of yellow color,\n" +
                        "I will drop soon,\n" +
                        "At any hour",
                "pear",
                "I'm a fruit",
                R.drawable.level4_3_3);

        Riddle r46 = new Riddle("r46",
                "I am a window\n" +
                        "Not belonging to a house",
                "glasses",
                "Helps the eyes",
                R.drawable.level4_4_1);

        Riddle r47 = new Riddle("r47",
                "What crosses a village\n" +
                        "And dogs won't bark?",
                "mist",
                "It's blurry",
                R.drawable.level4_4_2);

        Riddle r48 = new Riddle("r48",
                "All over the world you’ll find\n" +
                        "Five brothers of the same kind",
                "fingers",
                "They work together",
                R.drawable.level4_4_3);

        level4.add(r37);
        level4.add(r38);
        level4.add(r39);
        level4.add(r40);
        level4.add(r41);
        level4.add(r42);
        level4.add(r43);
        level4.add(r44);
        level4.add(r45);
        level4.add(r46);
        level4.add(r47);
        level4.add(r48);
    }

    public static int getLevelCompletionProgress(int level, Context context) {
        int countSolved = 0;
        List<Riddle> riddleList;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        if (level == 1) {
            riddleList = Levels.getLevel1();
        }
        else if (level == 2) {
            riddleList = Levels.getLevel2();
        }
        else if (level == 3) {
            riddleList = Levels.getLevel3();
        }
        else {
            riddleList = Levels.getLevel4();
        }

        for (Riddle riddle : riddleList) {
            String completed = preferences.getString(riddle.id, "N");
            if (completed.equals("Y")) {
                countSolved++;
            }
        }

        return countSolved;
    }
}
