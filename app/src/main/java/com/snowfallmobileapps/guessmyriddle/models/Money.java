package com.snowfallmobileapps.guessmyriddle.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by CGheorghisan on 9/3/2014.
 */
public class Money {

    public static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAln5XZ1EpOkYCTNoLTkKscUfqPe0Yj18znM9cKtfXHgUzjAJI0MAPEMOQ84FTLpp6O1EDxXqXbiSIMJSYyCryx47B9UhShgbbS+s9MnOL8sFYeNwu4SfYx+93zAG/Nq1F4DGOEKpUGSc4utmL8n22xciNcNgq9JMhKvEmYunFzje0KJ2BfNa6Ca+xhAbd6kBZ5d5SVtjhQRLXRlEiZ2hsceWhNcdNbASvgSjjEBA/LPaWWQB9iEti/g/Avp9+fGe+v0P55yYGM2oorrtWAYv7Zz5GYCu/mQ2uM4Jw/3bPMgMmc/JBj2E24NISgzpIV5ZysWFQ0gd4iIqpkWpk56s4TQIDAQAB";

    public static final String BAG_OF_MONEY = "BagOfMoney";
    public static final int MONEY_RIDDLE_GAIN = 20;
    public static final int MONEY_HINT_COST = 20;
    public static final int MONEY_LETTER_COST = 10;
    public static final int MONEY_LEVEL_GAIN = 30;

    public static int getBagOfMoney(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        int bagOfMoney = preferences.getInt(BAG_OF_MONEY, 50);

        return  bagOfMoney;
    }

    public static void increaseBagOfMoney(int amount, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        //get the current bag of money
        int bagOfMoney = preferences.getInt(BAG_OF_MONEY, 50);
        //increase it
        bagOfMoney = bagOfMoney + amount;

        //store the updated value
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(BAG_OF_MONEY, bagOfMoney);
        editor.apply();
    }

    public static void decreaseBagOfMoney(int amount, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        //get the current bag of money
        int bagOfMoney = preferences.getInt(BAG_OF_MONEY, 50);
        //increase it
        bagOfMoney = bagOfMoney - amount;

        //store the updated value
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(BAG_OF_MONEY, bagOfMoney);
        editor.apply();
    }
}
