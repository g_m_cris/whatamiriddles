package com.snowfallmobileapps.guessmyriddle.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.PreferenceManager;

/**
 * Created by CGheorghisan on 8/28/2014.
 */
public class Riddle implements Parcelable {

    public String id; //r1, r2 etc
    public String text;
    public String answer;
    public String hint;
    public int puzzlePiece;
    public String completed = "N"; // Y

    public Riddle() {
        //empty constructor
    }

    public Riddle(String anId, String aText, String anAnswer, String aHint, int aPuzzlePiece) {
        this.id = anId;
        this.text = aText;
        this.answer = anAnswer;
        this.hint = aHint;
        this.puzzlePiece = aPuzzlePiece;
    }

    public static final Parcelable.Creator<Riddle> CREATOR = new Creator<Riddle>() {
        public Riddle createFromParcel(Parcel source) {
            Riddle riddle = new Riddle();
            riddle.id = source.readString();
            riddle.text = source.readString();
            riddle.answer = source.readString();
            riddle.hint = source.readString();
            riddle.completed = source.readString();

            return riddle;
        }
        public Riddle[] newArray(int size) {
            return new Riddle[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(id);
        parcel.writeString(text);
        parcel.writeString(answer);
        parcel.writeString(hint);
        parcel.writeString(completed);
    }

    public void retrieveRiddleCompletion(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String completed = preferences.getString(this.id, "N");
        this.completed = completed;
    }
}
