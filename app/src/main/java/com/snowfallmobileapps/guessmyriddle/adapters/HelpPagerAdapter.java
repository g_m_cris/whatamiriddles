/**
 * 
 */
package com.snowfallmobileapps.guessmyriddle.adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.snowfallmobileapps.guessmyriddle.R;

/**
 * @author mcgheorghisan
 *
 */
public class HelpPagerAdapter extends PagerAdapter {

	private static final int[] helpTextsIds = { R.string.help1,
		R.string.help2, R.string.help3,
		R.string.help4, R.string.help5,
        R.string.help6};
	
	private static final int[] helpDrawablesIds = { R.drawable.help1,
		R.drawable.help2, R.drawable.help3,
		R.drawable.help4, R.drawable.help5,
        R.drawable.help6};

@Override
public int getCount() {
	return helpTextsIds.length;
}

@Override
public Object instantiateItem(View container, int position) {
	Context context = container.getContext();
	LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	// switch in functie de pozitie
	// http://mobile.tutsplus.com/tutorials/android/android-user-interface-design-horizontal-view-paging/
	View view = inflater.inflate(R.layout.help_layout, null);
	TextView tv = (TextView) view.findViewById(R.id.helpTextView);
	ImageView imgView = (ImageView) view.findViewById(R.id.helpImageView);
	
	if(position < helpTextsIds.length) {
		tv.setText(context.getResources().getString(helpTextsIds[position]));
	}
	if(position < helpDrawablesIds.length) {
		imgView.setImageResource(helpDrawablesIds[position]);
	}

	((ViewPager) container).addView(view, 0);

	return view;
}

@Override
public boolean isViewFromObject(View arg0, Object arg1) {
	return arg0 == ((View) arg1);
}

@Override
public Parcelable saveState() {
	return null;
}

@Override
public void destroyItem(View arg0, int arg1, Object arg2) {
	((ViewPager) arg0).removeView((View) arg2);

}

}
