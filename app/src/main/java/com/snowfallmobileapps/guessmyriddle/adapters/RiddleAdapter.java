package com.snowfallmobileapps.guessmyriddle.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.snowfallmobileapps.guessmyriddle.models.Riddle;

import java.util.List;

import com.snowfallmobileapps.guessmyriddle.R;

/**
 * Created by CGheorghisan on 9/1/2014.
 */
public class RiddleAdapter extends BaseAdapter {
    private LayoutInflater mLayoutInflater;
    private int imageLayout;
    private List<Riddle> riddleList;
    private Context context;
    private String level;

    public RiddleAdapter(Context c, LayoutInflater aLayoutInflater, List<Riddle> aRiddleList, String aLevel) {
        context= c;
        mLayoutInflater = aLayoutInflater;
        riddleList = aRiddleList;
        level = aLevel;
    }

    public int getCount() {
        return riddleList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            convertView = mLayoutInflater.inflate(R.layout.element_riddles_list, null);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.riddleTextView);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.riddleImageView);

        Riddle riddle = riddleList.get(position);
        riddle.retrieveRiddleCompletion(context);

        textView.setText(riddle.text);

        if (riddle.completed.equals("N")) {
            //display question mark
            imageView.setImageDrawable(convertView.getResources().getDrawable(R.drawable.question_mark_2));
        }
        else {
            //display check mark
            imageView.setImageDrawable(convertView.getResources().getDrawable(R.drawable.transparentgreencheckmark));
        }

        return convertView;
    }
}
